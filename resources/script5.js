const image = document.querySelector("#image");
const range = document.querySelector("#range input");
const glass = document.querySelector("#glass");
const eventLayer = document.querySelector("#eventLayer");

let imgBorder = (image.offsetWidth - image.clientWidth) /2;
let imgLeft = image.offsetLeft + imgBorder;
let imgTop = image.offsetTop + imgBorder;
let cond = false;

console.log(image.offsetWidth)
console.log(image.clientWidth)
console.log(imgBorder)
console.log(imgLeft)

window.addEventListener("resize", () =>{
    imgLeft = image.offsetLeft + imgBorder;
    imgTop = image.offsetTop + imgBorder;
});

range.addEventListener("input", () =>{
    glass.style.setProperty("--m", range.value);
});

eventLayer.addEventListener("click", () =>{
    if (cond) cond = false;
    else cond = true;
})
eventLayer.addEventListener("mousemove", (ev) =>{

    if (cond) {
        glass.style.setProperty("--x", `${ev.x - imgLeft}px`);
        glass.style.setProperty("--y", `${ev.y - imgTop}px`);
    }
})

var cursor = document.getElementById("cursor");
document.body.addEventListener("mousemove", function(ev) {
cursor.style.left = `${ev.x}px`;
cursor.style.top = `${ev.y}px`;
});

range.addEventListener("input", () =>{
    cursor.style.setProperty("--m", range.value);
});
